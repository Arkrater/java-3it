package com.ark.springrest.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ark.springrest.Model.Usuario;


public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	Optional<Usuario> findById(Long id);

}
