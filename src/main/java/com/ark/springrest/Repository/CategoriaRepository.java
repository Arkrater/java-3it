package com.ark.springrest.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ark.springrest.Model.Categoria;


public interface CategoriaRepository extends CrudRepository<Categoria, Long>{
	Optional<Categoria> findById(Long id);
	
	Iterable<Categoria> findAll();

}
