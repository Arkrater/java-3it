package com.ark.springrest.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ark.springrest.Model.Respuesta;


public interface RespuestaRepository extends CrudRepository<Respuesta, Long>{
	Optional<Respuesta> findById(Long id);

}
