package com.ark.springrest.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ark.springrest.Model.Respuesta;
import com.ark.springrest.Repository.RespuestaRepository;



@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/respuestas")
public class RespuestaController {
	
	@Autowired
	RespuestaRepository repository;
	
	@PostMapping(value = "/create")
	public Respuesta postCustomer(@RequestBody Respuesta respuesta) {

		Respuesta respuestaPost = repository.save(new Respuesta(respuesta.getPregunta(), respuesta.getRespuesta(), respuesta.getEmail()));
		return respuestaPost;
	}
	

}
