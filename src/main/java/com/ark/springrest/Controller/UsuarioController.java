package com.ark.springrest.Controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ark.springrest.Model.Usuario;
import com.ark.springrest.Repository.UsuarioRepository;



@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {

	@Autowired
	UsuarioRepository repository;

	
	@PostMapping(value = "/crear")
	public Usuario postUsuario(@RequestBody Usuario usuario) {
		
		Usuario usuarioPost = repository.save(new Usuario(usuario.getEmail()));
		return usuarioPost;
	}
	
}
