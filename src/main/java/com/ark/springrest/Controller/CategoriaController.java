package com.ark.springrest.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ark.springrest.Model.Categoria;
import com.ark.springrest.Repository.CategoriaRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/categorias")
public class CategoriaController {

	@Autowired
	CategoriaRepository repository;

	
	@GetMapping("/categorias")
	public List<Categoria> findAllCategories() {
		System.out.println("Obteniendo Categorias");

		List<Categoria> categorias = new ArrayList<Categoria>();
		repository.findAll().forEach(categorias::add);

		return categorias;
	}
}
