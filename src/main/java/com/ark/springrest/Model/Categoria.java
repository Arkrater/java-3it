package com.ark.springrest.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categoria")
public class Categoria implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "categoria")
	private String categoria;
	
	public Categoria() {
	}

	public Categoria(String categoria) {
		this.categoria = categoria;
		
	}

	public long getId() {
		return id;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCategoria() {
		return this.categoria;
	}
	
	@Override
	public String toString() {
		return "Categoria [id=" + id + ", categoria=" + categoria  + "]";
	}
	
	
}
