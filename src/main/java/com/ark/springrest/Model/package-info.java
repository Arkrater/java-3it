/**
 * Este Modulo se encarga de las entidades requeridas por el proyecto, permitiendo
 * crear la base de datos segun las etiquetas y el nombre otorgado de la futura tabla
 * @since 0.1
 * @author Claudio Nuñez
 * @version 0.1
 */ 

package com.ark.springrest.Model;