package com.ark.springrest.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "respuesta")
public class Respuesta implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "pregunta")
	private String pregunta;
	
	@Column(name = "respuesta")
	private String respuesta;
	
	@Email
	@Column(name = "email")
	private String email;
	
	public Respuesta() {
	}

	public Respuesta(String pregunta, String respuesta, String email) {
		this.pregunta = pregunta;
		this.respuesta = respuesta;
		this.email = email;
		
	}

	public long getId() {
		return id;
	}
	
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getPregunta() {
		return this.pregunta;
	}
	

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getRespuesta() {
		return this.respuesta;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return this.email;
	}
	
	@Override
	public String toString() {
		return "Respuesta [id=" + id + ", respuesta=" + respuesta  + "]";
	}
	
	
}
