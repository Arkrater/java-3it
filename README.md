## Tecnologias Utilizadas

-- Backend --
1. Java: jdk-8u291
2. Springboot 4-4.10.0 (IDE)
3. Maven: 3.8.1
4. BBDD: In memory database h2 
---

## Instalacion

1. Descargar el proyecto 3it
2. Importar como proyecto Maven al IDE, en mi caso spring-tool-suite-4-4.10.0
3. Realizar update project por las dependencias en caso de no realizar descarga automatica.
4. Compilar el proyecto con maven clean install
5. Levantar App con el mismo server de springboot
6. Revisar en la raíz del disco en la ruta C:/3it/h2 si se ha creado la bbdd
7. Ingresar a http://localhost:8080/h2_console/login.jsp y realizar test de conexion a la bbdd

## Version

0.0.1

## Motivo del proyecto

Este proyecto se realizó con el objetivo de entregar un producto MVP para la entrevista técnica en 3IT, 
donde el desafío fue desarrollar una pagina de encuestas con integración de 
Angular como Frontend y Java Springboot como Backend con almacenado en una base de datos en memoria (H2)